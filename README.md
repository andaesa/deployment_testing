This repository contains testing script of installing certain packages and library on ubuntu server using Ansible.

1.create new virtual environment inside the root directory.
2.activate the virtual environment.
3. pip install ansible
4. run /.deploy_prod.sh

[Update 18 January]

Tested on fresh digitalocean server. Overall both fabric script and ansible playbook runs well.

[Solved Issue]
Apparently for fresh server, there is no pre-installed python 2.7.There will be an error saying that it can't find python directory.
To solved this, I installed the python package before running the ansible playbook.
(Can write in fabric)
Another error is while installing psycopg2.I installed the psycopg2 manually by using pip and rerun the ansible playbook.
(Can write in fabric?)

[Unsolved Issue]
Apparently there is an error regarding the main.yml in subdirectory role/common/handlers.
The reason and how to solved this error is still unknown. The error is for restarting the nginx service.

[Update 19 January]
All issue is solved. Fabric file and Ansible playbook 100% working. Tested on newly rebuilt droplet. 
